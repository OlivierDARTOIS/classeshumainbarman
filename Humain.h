#pragma once
#include <string>

class Humain
{
protected:
	std::string nom;
	std::string boissonFavorite;
public:
	Humain();
	~Humain();
	std::string getNom() const;
	std::string getBoissonFavorite() const;
	void setBoissonFavorite(std::string boisson);
	void parle();
	virtual std::string sePresente();
	void boit();
};

