#include <iostream>
#include "Humain.h"

Humain::Humain()
{
	std::cout << "Dans constructeur Humain..." << std::endl;
	this->nom = "";
	this->boissonFavorite = "";
}

Humain::~Humain()
{
	std::cout << "Dans destructeur Humain..." << std::endl;
}

std::string Humain::getNom() const
{
	return this->nom;
}

std::string Humain::getBoissonFavorite() const
{
	return this->boissonFavorite;
}

void Humain::setBoissonFavorite(std::string boisson)
{
	this->boissonFavorite = boisson;
}

void Humain::parle()
{ }

std::string Humain::sePresente()
{
	return "Methode sePresente classe Humain";
}

void Humain::boit()
{ }
