#include <iostream>
#include "Barman.h"

Barman::Barman(const std::string nom, const std::string boissonFavorite, const std::string nomBar) : Humain()
{
	std::cout << "Dans constructeur Barman..." << std::endl;
	Humain::nom = nom;
	Humain::setBoissonFavorite(boissonFavorite);
	this->nomBar = nomBar;
}

Barman::~Barman()
{
	std::cout << "Dans destructeur Barman..." << std::endl;
}

std::string Barman::getNomBar() const
{
	return this->nomBar;
}

void Barman::parle(const std::string texte)
{ }

std::string Barman::sePresente()
{
	return "Methode sePresente classe Barman";
}

void Barman::sert(const Humain& client)
{ }