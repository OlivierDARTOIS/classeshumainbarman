#include <iostream>
#include "Barman.h"

int main()
{
	Humain h1;
	Barman b1("Arthur", "Wisky", "chezMoi");
	std::cout << h1.getNom() << std::endl;
	std::cout << b1.getNomBar() << std::endl;

	std::cout << b1.getNom() << std::endl;
	
	std::cout << b1.getBoissonFavorite() << std::endl;
	b1.setBoissonFavorite("Vodka");
	std::cout << b1.getBoissonFavorite() << std::endl;

	// impossible de changer le nom du barman b1, pas de m�thode setNom !!

	Humain* ph = &b1;
	std::cout << ph->sePresente() << std::endl;

	return 0;
}