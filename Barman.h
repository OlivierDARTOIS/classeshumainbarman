#pragma once
#include "Humain.h"

class Barman : public Humain
{
private:
	std::string nomBar;
public:
	Barman(const std::string nom = "", const std::string boissonFavorite = "biere", const std::string nomBar = "");
	~Barman();
	std::string getNomBar() const;
	void parle(const std::string texte);
	std::string sePresente();
	void sert(const Humain& client);
private:
	std::string terminePhrase();
};

